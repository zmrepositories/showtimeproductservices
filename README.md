# ShowTime Product Services


## feature list
* maven project
* uses Jersey rest full frame work
* uses eclipse persistence api
* uses javax EJB
* build on GlassFish


## TODO
Jersey media multi part artifact



## important

notice set in the header the flowing information anny option is possible get json retrieve
xml or reverse or both types. Set the _Accept_ and _Content-Type_ in wat you like
```sh
Accept: application/json
Content-Type: application/json
```
or
```sh
Accept: application/xml
Content-Type: application/xml
```

notice:   check if the __persistence.xml__ is been deployed!


## URL's

### HEAD: get all products head including amount of products
#### http://94.214.159.219:7575/showtime/services/products

### GET: get all products
#### http://domain:8080/showtime/services/products

### GET: get all products by limit
#### http://domain:8080/showtime/services/products/{limit}


### GET: get product by id
#### http://domain:8080/showtime/services/product/id/{id}


### GET: get product by productId (exturnal id)
#### http://94.214.159.219:7575/showtime/services/product/pid/{pid}


### GET: get product by ean (europe article number)
#### http://domain:8080/showtime/services/product/ean/{ean}


### GET: search for a product with title or part of a title (case insensitive)
#### http://domain:8080/showtime/services/product/search/{title}


### GET: search for a product with year and title or part of a title (case insensitive)
#### http://domain:8080/showtime/services/product/search/{title}/{year}


### GET: search for a products with title or part of a title (case insensitive)
#### http://domain:8080/showtime/services/products/search/{title}


### GET: search for a products with year title or part of a title (case insensitive)
#### http://domain:8080/showtime/services/products/search/{title}/{year}


### POST: create a new product
#### http://domain:8080/showtime/services/product


### POST: add multiple products at a time
#### http://domain:8080/showtime/services/products


### PUT: update a product
#### http://domain:8080/showtime/services/product


### DELETE: delete a product on id
#### http://domain:8080/showtime/services/product/id/{id}


### DELETE: delete a product on ean (europe article number)
#### http://domain:8080/showtime/services/product/ean/{ean}


### DELETE: delete a product on productId (exturnal id)
#### http://94.214.159.219:7575/showtime/services/product/pid/{pid}