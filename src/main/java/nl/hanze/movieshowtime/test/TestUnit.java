package nl.hanze.movieshowtime.test;

import nl.hanze.movieshowtime.entities.Product;


import javax.persistence.*;
import java.util.List;





public class TestUnit
{





    public static void main(String[] args) throws Exception
    {


        EntityManagerFactory emfactory  = Persistence.createEntityManagerFactory("ShowTimeProductServiceUnit");
        EntityManager manager = emfactory.createEntityManager();



        Query query = manager.createQuery("SELECT OBJECT(p) FROM nl.hanze.movieshowtime.entities.Product p");

        List<Product> products = query.getResultList();


        for (Product product : products) {
            System.out.println(product);
        }
        System.out.println("Size: " + products.size());
/*
        // create new todo
        em.getTransaction().begin();
        Todo todo = new Todo();
        todo.setSummary("This is a test");
        todo.setDescription("This is a test");
        em.persist(todo);
        em.getTransaction().commit();*/

        //em.close();
    }
}
