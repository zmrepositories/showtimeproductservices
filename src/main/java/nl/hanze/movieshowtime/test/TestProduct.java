package nl.hanze.movieshowtime.test;

import nl.hanze.movieshowtime.entities.Product;
import nl.hanze.movieshowtime.services.response.Fault;
import nl.hanze.movieshowtime.services.response.Item;
import nl.hanze.movieshowtime.services.response.Message;
import nl.hanze.movieshowtime.services.response.Success;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;


public class TestProduct
{
    public static void testUnMarshaling() throws JAXBException, IOException
    {
        JAXBContext context = JAXBContext.newInstance(Product.class);

        Unmarshaller unmarshaller = context.createUnmarshaller();
        Product product = (Product) unmarshaller.unmarshal(new FileReader(System.getProperty("user.dir")+"\\src\\main\\java\\nl\\hanze\\movieshowtime\\test\\product.xml"));



        System.out.println(product);
    }


    public static void testMarshaling() throws JAXBException, IOException
    {
        Message msg = new Message();
        msg.addFaultItem(new Fault(500, "Failed to create product, object already exists!").setItem(new Item(345345L, "title test 1")) );
        msg.addFaultItem(new Fault(500, "Failed to create product, object already exists!").setItem(new Item(855676L, "title test 2")) );
        msg.addSuccessItem(new Success(200, "Product is successfully created").setItem(new Item(8354645L, "title test 2")));
        msg.addSuccessItem(new Success(200, "Product is successfully created").setItem(new Item(2754644L, "title test 4")) );

        File file = new File(System.getProperty("user.dir")+"\\src\\main\\java\\nl\\hanze\\movieshowtime\\test\\message.xml");
        JAXBContext jaxbContext = JAXBContext.newInstance(Message.class);
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

        // output pretty printed
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

        jaxbMarshaller.marshal(msg, file);
        jaxbMarshaller.marshal(msg, System.out);

        System.out.println(msg);
    }


    public static void main(String[] args) throws Exception{


        TestProduct.testMarshaling();

    }

}
