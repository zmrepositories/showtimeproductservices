package nl.hanze.movieshowtime.services;

import org.eclipse.persistence.jaxb.rs.MOXyJsonProvider;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;


@ApplicationPath("services")
public class RESTApp extends Application
{
    @Override
    public Set<Class<?>> getClasses()
    {
        HashSet<Class<?>> set = new HashSet<Class<?>>();
        set.add(MOXyJsonProvider.class);
        set.add(ProductServices.class);
        set.add(CategoryServices.class);
        return set;
    }
}
