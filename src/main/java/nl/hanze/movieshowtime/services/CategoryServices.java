package nl.hanze.movieshowtime.services;

import nl.hanze.movieshowtime.beans.CategoryBean;
import nl.hanze.movieshowtime.beans.ProductBean;
import nl.hanze.movieshowtime.entities.Category;
import nl.hanze.movieshowtime.entities.Product;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;


@Path("/")
@Stateless()
public class CategoryServices
{

    @EJB
    CategoryBean categoryManager;



    /**
     * @link http://localhost:8080/showtime/services/categories
     *
     * @return List<Category>
     */
    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Path("categories")
    @SuppressWarnings("unchecked")
    public List<Category> getAllCategories()
    {
        return (List<Category>)categoryManager.getAll();
    }


    /**
     * @link http://localhost:8080/showtime/services/category/{id}
     *
     * @param id long
     * @return Response
     */
    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Path("category/{id}")
    public Response getCategoryById(@PathParam("id") long id)
    {
        return Response.status(Response.Status.OK).entity( categoryManager.getById(id) ).build();
    }

}
