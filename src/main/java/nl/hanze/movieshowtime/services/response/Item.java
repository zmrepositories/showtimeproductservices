package nl.hanze.movieshowtime.services.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="product")
public class Item
{
    @XmlElement(name="ean")
    private long ean;

    @XmlElement(name="title")
    private String title;

    public Item(){}

    public Item(long ean, String title)
    {
        this.ean = ean;
        this.title = title;
    }

    public long getEAN()
    {
        return ean;
    }

    public String getTitle()
    {
        return title;
    }

    public String toString()
    {
        return String.format("\n\t  Product: [\n " +
                "\t\t  ean: %s " +
                "\n\t\t  title: %s " +
                "\n\t  ]",ean, title);
    }
}
