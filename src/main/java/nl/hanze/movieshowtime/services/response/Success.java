package nl.hanze.movieshowtime.services.response;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="success")
public class Success
{
    @XmlElement(name="responseCode")
    private int code;

    @XmlElement(name="responseMessage")
    private String message;


    @XmlElement(name="product")
    private Item item;


    public Success(){}

    public Success(int code, String message)
    {
        this();
        this.code    = code;
        this.message = message;
    }

    public String getMessage()
    {
        return message;
    }

    public int getCode()
    {
        return code;
    }

    public Success setItem(Item item)
    {
        this.item = item;
        return this;
    }

    public Item getItem()
    {
        return item;
    }

    public String toString()
    {
        return String.format("\n\tSuccess: [\n " +
                "\t  code: %s " +
                "\n\t  message: %s " +
                "%s"+
                "\n\t]",code, message, ((item == null)?   "": item));
    }
}