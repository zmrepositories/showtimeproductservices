package nl.hanze.movieshowtime.services;

import nl.hanze.movieshowtime.beans.ProductBean;
import nl.hanze.movieshowtime.entities.Product;
import nl.hanze.movieshowtime.entities.ProductCollection;
import nl.hanze.movieshowtime.services.response.Fault;
import nl.hanze.movieshowtime.services.response.Item;
import nl.hanze.movieshowtime.services.response.Message;
import nl.hanze.movieshowtime.services.response.Success;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;



/**
 *
 * @version 1.0
 * @author Michel Tartarotti
 */
@Path("/")
@Stateless()
public class ProductServices
{

    @Context
    private HttpServletResponse servletResponse;

    @EJB
    ProductBean productManager;


    @GET
    @Path("test")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response doTest()
    {
        return Response.status(Response.Status.OK).entity(new Message("test", "test successfully executed 1:)")).build();
    }



    /**
     * @return List<Product>
     * @link http://localhost:8080/showtime/services/products
     */
    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Path("products")
    @SuppressWarnings("unchecked")
    public Response getAllProducts()
    {
        List<Product> products = productManager.getAll();
        servletResponse.setHeader("Product-size", products.size() + "");
        return Response.ok(products.toArray(new Product[products.size()])).build();
    }

    /**
     *
     * @param limit int
     * @return List<Product>
     * @link http://localhost:8080/showtime/services/products
     */
    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Path("products/{limit}")
    @SuppressWarnings("unchecked")
    public Response getAllProductsLimit(@PathParam("limit") int limit)
    {
        List<Product> products = productManager.getAllWithLimit(limit);
        servletResponse.setHeader("Product-size", products.size() + "");

        return Response.ok(products.toArray(new Product[products.size()])).build();
    }


    /**
     * @return List<Product>
     * @link http://localhost:8080/showtime/services/products
     */
    @HEAD
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Path("products")
    @SuppressWarnings("unchecked")
    public Response getAllProductsHead()
    {
        List<Product> products = productManager.getAll();
        servletResponse.setHeader("Product-size", products.size() + "");
        return Response.ok(products.toArray(new Product[products.size()])).build();
    }

    /**
     * method makes it possible to create more than one new products
     *
     * @param products ProductCollection
     * @return Response
     */
    @POST
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Path("products")
    @SuppressWarnings("unchecked")
    public Response addProducts(ProductCollection products, @HeaderParam("Product-size") int size)
    {
        Response.Status status = Response.Status.CREATED;
        Message msg = new Message();
        int errorCount = 0;
        int successCount = 0;

        //TODO !workaround four double product send by client
        if(size > 0){
            products.splice(0, size);
        }

        for(Product product : products.getAll())
        {
            if(product !=null && productManager.getByEAN(product.getEan()) == null)
            {
                product = productManager.create(product);
                if(product.exists()) {
                    msg.addSuccessItem(new Success(Response.Status.CREATED.getStatusCode(), "Product is successfully created").setItem(new Item(product.getEan(), product.getTitle())) );
                    successCount++;
                }else{
                    status = Response.Status.INTERNAL_SERVER_ERROR;
                    msg.addFaultItem(new Fault(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode(), "Failed to create product, internal error!").setItem(new Item(product.getEan(), product.getTitle())) );
                    errorCount++;
                }
            }else{
                msg.addFaultItem(new Fault(Response.Status.CONFLICT.getStatusCode(), "Failed to create product, object already exists!").setItem(new Item(product.getEan(), product.getTitle())) );
                errorCount++;
            }
        }

        // if there are anny items added with success de action has failed completely...
        if(errorCount > 0 && successCount == 0){
            status = Response.Status.CONFLICT;
        }
        return Response.status(status).entity(msg).build();
    }


    /**
     * @param id long
     * @return Response
     * @link http://localhost:8080/showtime/services/product/id/{id}
     */
    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Path("product/id/{id}")
    public Response getProductById(@PathParam("id") long id) {

        Response.Status status  = Response.Status.OK;
        Product product         = productManager.getById(id);
        Object entity;

        if(product !=null && product.exists()){
            entity = product;
        }else{
            status = Response.Status.NOT_FOUND;
            entity = new Message().setTitle("Product not found").setText(String.format("The product can not be found by id[%s]", id));
            }

        return Response.status(status).entity(entity).build();
    }


    /**
     * @param productId String
     * @return Response
     * @link http://localhost:8080/showtime/services/product/pid/{pid}
     */
    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Path("product/pid/{pid}")
    public Response getProductByProductId(@PathParam("pid") String productId) {

        Response.Status status  = Response.Status.OK;
        Product product         = productManager.getByProductId(productId);
        Object entity;

        if(product !=null && product.exists()){
            entity = product;
        }else{
            status = Response.Status.NOT_FOUND;
            entity = new Message().setTitle("Product not found").setText(String.format("The product can not be found by productId[%s]", productId));
        }

        return Response.status(status).entity(entity).build();
    }


    /**
     * @param ean long
     * @return Response
     * @link http://localhost:8080/showtime/services/product/ean/{ean}
     */
    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Path("product/ean/{ean}")
    public Response getProductByEAN(@PathParam("ean") long ean) {

        Response.Status status  = Response.Status.OK;
        Product product         = productManager.getByEAN(ean);
        Object entity;

        if(product !=null && product.exists()){
            entity = product;
        }else{
            status = Response.Status.NOT_FOUND;
            entity = new Message().setTitle("Product not found").setText(String.format("The product can not be found by ean[%s]", ean));
        }

        return Response.status(status).entity(entity).build();
    }




    /**
     * @param title String
     * @return Response
     * @link http://localhost:8080/showtime/services/product/search/{title}
     */
    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Path("product/search/{title}")
    public Response searchByTitle(@PathParam("title") String title)
    {

        Response.Status status  = Response.Status.OK;
        Product product         = productManager.searchByTitle(title);
        Object entity;

        if(product !=null && product.exists()){
            entity = product;
        }else{
            status = Response.Status.NOT_FOUND;
            entity = new Message().setTitle("Product not found").setText(String.format("There is no product found on the search criterium [%s]", title));
        }
        return Response.status(status).entity(entity).build();
    }



    /**
     * @param title String
     * @return Response
     * @link http://localhost:8080/showtime/services/product/search/{title}/{year}
     */
    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Path("product/search/{title}/{year}")
    public Response searchByTitleAndYear(@PathParam("title") String title, @PathParam("year") int year)
    {

        Response.Status status  = Response.Status.OK;
        Product product         = productManager.searchByTitleAndYear(title, year);
        Object entity;

        if(product !=null && product.exists()){
            entity = product;
        }else{
            status = Response.Status.NOT_FOUND;
            entity = new Message().setTitle("Product not found").setText(String.format("There is no product found on the search criterium [%s] and the year[%s]", title, year));
        }
        return Response.status(status).entity(entity).build();
    }



    /**
     * @return List<Product>
     * @link http://localhost:8080/showtime/services/products/search/{title}
     */
    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Path("products/search/{title}")
    @SuppressWarnings("unchecked")
    public Response searchAllByTitle(@PathParam("title") String title)
    {
        Response response;
        List<Product> products = productManager.searchAllByTitle(title);
        servletResponse.setHeader("Product-size", products.size()+"");

        if(products.size() > 0)
        {
            response = Response.ok(products.toArray(new Product[products.size()])).build();
        }else{
            Message msg = new Message().setTitle("Products not found").setText(String.format("There are no products found on the search criterium [%s]", title));
            response = Response.status(Response.Status.NOT_FOUND).entity(msg).build();
        }
        return response;
    }



    /**
     * @return List<Product>
     * @link http://localhost:8080/showtime/services/products/search/{title}/{year}
     */
    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Path("products/search/{title}/{year}")
    @SuppressWarnings("unchecked")
    public Response searchAllByTitleAndYear(@PathParam("title") String title, @PathParam("year") int year)
    {
        Response response;
        List<Product> products = productManager.searchAllByTitleAndYear(title, year);
        servletResponse.setHeader("Product-size", products.size()+"");

        if(products.size() > 0)
        {
            response = Response.ok(products.toArray(new Product[products.size()])).build();
        }else{
            Message msg = new Message().setTitle("Products not found").setText(String.format("There are no products found on the search criterium [%s] and the year[%s]", title, year));
            response = Response.status(Response.Status.NOT_FOUND).entity(msg).build();
        }
        return response;
    }



    /**
     * consumes xml as json by uri
     *
     * @param product Actor
     * @link http://localhost:8080/showtime/services/product
     */
    @POST
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Path("product")
    public Response createProduct(Product product)
    {
        Response.Status status = Response.Status.CREATED;
        Message msg = new Message();

        if(product !=null && productManager.getByEAN(product.getEan()) == null)
        {
            product = productManager.create(product);

            if (product.exists()) {
                msg.success().setId(product.getId()).setText("[" + product.getClass().getName() + "] successfully created");
            } else {
                status = Response.Status.INTERNAL_SERVER_ERROR;
                msg.failed().setText("Failed to create [" + product.getClass().getName() + "]");
            }
        }else{
            status = Response.Status.CONFLICT;
            msg.failed().setText("Failed to create [" + product.getClass().getName() + "] product already exists!");
        }

        return Response.status(status).entity(msg).build();
    }



    /**
     * consumes xml as json by uri
     *
     * @param product Product
     * @link http://localhost:8080/showtime/services/product
     */
    @PUT
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Path("product")
    public Response updateProduct(Product product) {

        Response.Status status = Response.Status.OK;
        Message msg = new Message();

        if (productManager.update(product)) {
            msg.success().setId(product.getId()).setText("[" + product.getClass().getName() + "] is successfully updated");
        } else {
            status = Response.Status.NOT_MODIFIED;
            msg.failed().setId(product.getId()).setText("Failed to update [" + product.getClass().getName() + "]");
        }

        return Response.status(status).entity(msg).build();
    }



    /**
     * produces xml as json by uri
     *
     * @param id long
     * @link http://localhost:8080/showtime/services/product/id/{id}
     */
    @DELETE
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Path("product/id/{id}")
    public Response deleteOnId(@PathParam("id") long id) {

        Response.Status status = Response.Status.OK;
        Message msg = new Message();

        if (productManager.deleteById(id)) {
            msg.success().setId(id).setText("successfully deleted");
        } else {
            status = Response.Status.NOT_MODIFIED;
            msg.failed().setId(id).setText("Failed to delete");
        }

        return Response.status(status).entity(msg).build();
    }



    /**
     * produces xml as json by uri
     *
     * @param productId String
     * @link http://localhost:8080/showtime/services/product/ean/{id}
     */
    @DELETE
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Path("product/pid/{pid}")
    public Response deleteOnProductId(@PathParam("pid") String productId) {

        Response.Status status = Response.Status.OK;
        Message msg = new Message();

        if (productManager.deleteByProductId(productId)) {
            msg.success().setText("successfully deleted");
        } else {
            status = Response.Status.NOT_MODIFIED;
            msg.failed().setText(String.format("Failed to delete productId:[%s]", productId));
        }

        return Response.status(status).entity(msg).build();
    }


    /**
     * produces xml as json by uri
     *
     * @param ean long
     * @link http://localhost:8080/showtime/services/product/ean/{id}
     */
    @DELETE
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Path("product/ean/{ean}")
    public Response deleteOnEan(@PathParam("ean") long ean) {

        Response.Status status = Response.Status.OK;
        Message msg = new Message();

        if (productManager.deleteByEan(ean)) {
            msg.success().setId(ean).setText("successfully deleted");
        } else {
            status = Response.Status.NOT_MODIFIED;
            msg.failed().setId(ean).setText("Failed to delete");
        }

        return Response.status(status).entity(msg).build();
    }

}
