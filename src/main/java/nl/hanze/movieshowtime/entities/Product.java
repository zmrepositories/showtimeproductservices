package nl.hanze.movieshowtime.entities;


import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;


@Entity(name="product")
@Table(name="product")
@XmlRootElement(name="product")
@NamedQueries({
        @NamedQuery(name="Product.searchOnTitle", query="SELECT OBJECT(p) FROM nl.hanze.movieshowtime.entities.Product p WHERE lower(p.title) LIKE lower(:title)"),
        @NamedQuery(name="Product.searchOnTitleAndYear", query="SELECT OBJECT(p) FROM nl.hanze.movieshowtime.entities.Product p WHERE lower(p.title) LIKE lower(:title) AND p.year=:year")
})
public class Product implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private long id;


    @Column(name = "product_id")
    private String productId;


    @Column(name = "ean")
    private long ean;


    @Column(name = "title")
    private String title;


    @Column(name = "gpc")
    private String gpc;


    @Column(name = "availability_code")
    private int availabilityCode;


    @Column(name = "availability_description")
    private String availabilityDescription;


    @Column(name = "cond")
    private String condition;


    @Column(name = "price")
    private BigDecimal price;


    @Column(name = "year")
    private int year;


    @Column(name = "origin")
    private String origin;


    /**
     * EAGER: Convenient, but slow
     * LAZY: More coding, but much more efficient
     */

    @OneToMany(targetEntity=ProductCategory.class, mappedBy="product", fetch=FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "product_category", joinColumns = @JoinColumn(name = "product_id"), inverseJoinColumns = @JoinColumn(name = "id"))
    private List<ProductCategory> categories;


    @OneToMany(targetEntity=ProductSeller.class, mappedBy="product", fetch=FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "product_seller", joinColumns = @JoinColumn(name = "product_id"), inverseJoinColumns = @JoinColumn(name = "id"))
    private List<ProductSeller> sellers;

    public Product() {
    }


    public void setId(int id) {
        this.id = id;
    }


    @XmlElement(name = "categories")
    public List<ProductCategory> getCategories() {
        return categories;
    }


    @XmlElement(name = "sellers")
    public List<ProductSeller> getSellers() {
        return sellers;
    }


    @XmlElement(name = "id")
    public long getId()
    {
        return id;
    }


    public void setId(long id)
    {
        this.id = id;
    }


    @XmlElement(name = "productId")
    public String getProductId()
    {
        return productId;
    }


    public Product setProductId(String productId)
    {
        this.productId = productId;
        return this;
    }


    @XmlElement(name = "ean")
    public long getEan()
    {
        return ean;
    }


    public Product setEan(long ean)
    {
        this.ean = ean;
        return this;
    }


    @XmlElement(name = "gpc")
    public String getGpc()
    {
        return gpc;
    }


    public Product setGpc(String gpc)
    {
        this.gpc = gpc;
        return this;
    }


    @XmlElement(name = "title")
    public String getTitle()
    {
        return title;
    }


    public Product setTitle(String title)
    {
        this.title = title;
        return this;
    }


    @XmlElement(name = "availabilityCode")
    public int getAvailabilityCode()
    {
        return availabilityCode;
    }


    public Product setAvailabilityCode(int availabilityCode)
    {
        this.availabilityCode = availabilityCode;
        return this;
    }


    @XmlElement(name = "availabilityDescription")
    public String getAvailabilityDescription()
    {
        return availabilityDescription;
    }


    public Product setAvailabilityDescription(String availabilityDescription)
    {
        this.availabilityDescription = availabilityDescription;
        return this;
    }


    @XmlElement(name = "condition")
    public String getCondition()
    {
        return condition;
    }

    public Product setCondition(String condition)
    {
        this.condition = condition;
        return this;
    }


    @XmlElement(name = "price")
    public BigDecimal getPrice()
    {
        return price;
    }

    public Product setPrice(BigDecimal price)
    {
        this.price = price;
        return this;
    }

    @XmlElement(name = "year")
    public int getYear()
    {
        return year;
    }

    public Product setYear(int year)
    {
        this.year = year;
        return this;
    }

    @XmlElement(name = "origin")
    public String getOrigin()
    {
        return origin;
    }

    public Product setOrigin(String origin)
    {
        this.origin = origin;
        return this;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Product product = (Product) o;

        if (availabilityCode != product.availabilityCode) return false;
        if (ean != product.ean) return false;
        if (id != product.id) return false;
        if (productId.equals(product.productId)) return false;
        if (availabilityDescription != null ? !availabilityDescription.equals(product.availabilityDescription) : product.availabilityDescription != null)
            return false;
        if (condition != null ? !condition.equals(product.condition) : product.condition != null) return false;
        if (price != null ? !price.equals(product.price) : product.price != null) return false;
        if (title != null ? !title.equals(product.title) : product.title != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = Integer.parseInt(((Long)id).toString());
        result = 31 * result + (int) (ean ^ (ean >>> 32));
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + availabilityCode;
        result = 31 * result + (availabilityDescription != null ? availabilityDescription.hashCode() : 0);
        result = 31 * result + (condition != null ? condition.hashCode() : 0);
        result = 31 * result + (price != null ? price.hashCode() : 0);
        return result;
    }


    public boolean exists()
    {
        return getId() > 0L && getEan() > 0L;
    }


    public String toString()
    {
        String categories = "";
        if(getCategories() != null){
            for(ProductCategory cat : getCategories())
            {
                categories += cat.getCategory().toString();
            }
        }
        return String.format("Product: [\n " +
                "\t    id: %s " +
                "\n\t    ProductId: %s " +
                "\n\t    EAN: %s " +
                "\n\t    title: %s " +
                "\n\t    gpc: %s " +
                "\n\t    availabilityCode: %s " +
                "\n\t    availabilityDescription: %s " +
                "\n\t    condition: %s " +
                "\n\t    price: %s " +
                "\n\t    origin: %s " +
                "\n\t    Categories: [\n%s " +
                "\n   ]",getId() ,getProductId(), getEan(), getTitle(),getGpc(), getAvailabilityCode(), getAvailabilityDescription(), getCondition(), getPrice(), getOrigin(), categories);
    }



}
