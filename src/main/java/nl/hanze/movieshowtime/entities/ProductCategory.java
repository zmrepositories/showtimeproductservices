package nl.hanze.movieshowtime.entities;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;


@Entity
@Table(name = "product_category", schema = "", catalog = "showtimeproducts")
//@XmlRootElement(name="productCategory")
@XmlAccessorType(XmlAccessType.NONE)
public class ProductCategory implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private long id;


    @Id
    @Column(name = "product_id")
    private long productId;


    @Id
    @Column(name = "category_id")
    private long categoryId;


    @ManyToOne
    @PrimaryKeyJoinColumn(name="category_id", referencedColumnName="id")
    private Category category;



    @ManyToOne
    @PrimaryKeyJoinColumn(name="product_id", referencedColumnName="id")
    private Product product;


    @XmlElement(name = "category")
    public Category getCategory(){
       return category;
    }


    public long getId() {
        return id;
    }

    public void setProductId(long productId)
    {
        this.productId = productId;
    }


    public long getProductId() {
        return productId;
    }


    public void setCategoryId(long categoryId)
    {
        this.categoryId = categoryId;
    }


    public long getCategoryId()
    {
        return categoryId;
    }


    public void setId(long id)
    {
        this.id = id;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProductCategory that = (ProductCategory) o;

        if (id != that.id) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Integer.parseInt(((Long)id).toString());
    }
}
