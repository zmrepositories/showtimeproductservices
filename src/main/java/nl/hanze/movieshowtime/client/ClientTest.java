package nl.hanze.movieshowtime.client;

/*
 * Class to test the client
 */

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.util.List;

import nl.hanze.movieshowtime.entities.Product;
import nl.hanze.movieshowtime.entities.ProductCollection;

public class ClientTest {

    /*
     * get all products from the showtime database
     */
    public static void getAllProducts() {
        ShowTimeClient client = new ShowTimeClient();

        @SuppressWarnings("unused")
        List<Product> list = client.getAllProducts();
    }

	/*
	 * get product from showtime database based on id with fixed value
	 */

    public static void getById() {
        ShowTimeClient client = new ShowTimeClient();

        Product product = client.getProductById(1L);

        if (client.getStatus() == 200) {
            System.out.println(product);
        } else {
            System.out.println(client.getStatus());
            System.out.println(client.getResponse());
        }
    }

	/*
	 * get product from showtime database based on ean fixed value
	 */

    public static void getByEan() {
        ShowTimeClient client = new ShowTimeClient();

        Product product = client.getProductByEan(8712609653694L);

        if (client.getStatus() == 200) {
            System.out.println(product);
        } else {
            System.out.println(client.getStatus());
            System.out.println(client.getResponse());
        }
    }

	/*
	 * get product from showtime database based on title fixed value
	 */

    public static void searchByTitle() throws UnsupportedEncodingException {
        ShowTimeClient client = new ShowTimeClient();

        Product product = client.searchProductByTitle("Avonturen Van Kuifje");

        if (client.getStatus() == 200) {
            System.out.println(product);
        } else {
            System.out.println(client.getStatus());
            System.out.println(client.getResponse());
        }
    }

	/*
	 * get product from showtime database based on title and year with fixed
	 * values
	 */

    public static void searchByTitleAndYear()
            throws UnsupportedEncodingException {
        ShowTimeClient client = new ShowTimeClient();

        Product product = client.searchProductByTitleAndYear("Harry Potter",
                2014);

        if (client.getStatus() == 200) {
            System.out.println(product);
        } else {
            System.out.println(client.getStatus());
            System.out.println(client.getResponse());
        }
    }

	/*
	 * add a new product to the showtime database with fixed values
	 */

    public static void addProduct() {

        ShowTimeClient client = new ShowTimeClient();

        Product product = new Product();

        product.setEan(9782203003040L).setProductId("2344345")
                .setTitle("Tintin Au Congo").setGpc("book")
                .setAvailabilityCode(175)
                .setAvailabilityDescription("3-5 werkdagen")
                .setCondition("Nieuw").setPrice(new BigDecimal(9.99))
                .setYear(2012);

        if (client.addProduct(product)) {
            System.out.println("Yes added :)");
        } else {
            System.out.println("No not added :(");
        }

        System.out.println(client.getStatus());
        System.out.println(client.getResponse());

    }

    /*
	 * add multiple new product to the showtime database with fixed values
	 */

    public static void addProducts()
    {
        ProductCollection collection = new ProductCollection();
        ShowTimeClient client        = new ShowTimeClient();

        // add product 1:...
        collection.add(
                new Product()
                        .setEan(118715664087268L)
                        .setProductId("1002004011756945")
                        .setTitle("BBC Earth - Planet Earth")
                        .setGpc("dvd")
                        .setAvailabilityCode(170)
                        .setAvailabilityDescription("Vandaag voor 23.00 uur besteld, morgen in huis")
                        .setCondition("Nieuw")
                        .setOrigin("bol.com")
                        .setYear(2011)
                        .setPrice(new BigDecimal(10.99))
        );
        // add product 2:...
        collection.add(
                new Product()
                        .setEan(118717418424565L)
                        .setProductId("9200000022838091")
                        .setTitle("Revenge - Seizoen 2")
                        .setGpc("dvd")
                        .setAvailabilityCode(170)
                        .setAvailabilityDescription("Vandaag voor 23.00 uur besteld, morgen in huis")
                        .setCondition("Nieuw")
                        .setOrigin("bol.com")
                        .setYear(2014)
                        .setPrice(new BigDecimal(29.99))
        );
        // add product 3:...
        collection.add(
                new Product()
                        .setEan(115051888210178L)
                        .setProductId("1002004011340435")
                        .setTitle("Band Of Brothers")
                        .setGpc("dvd")
                        .setAvailabilityCode(170)
                        .setAvailabilityDescription("Vandaag voor 23.00 uur besteld, morgen in huis")
                        .setCondition("Nieuw")
                        .setOrigin("bol.com")
                        .setYear(2011)
                        .setPrice(new BigDecimal(32.99))
        );

        if(client.addProducts(collection )){
            System.out.println("Yes success :)");
        }else{
            System.out.println("No failed :(");
        }


        System.out.println(client.getResponse());
    }


    /*
     * main class to run the tests
     */
    public static void main(String[] args) {
        try {

            // ClientTest.getByEan();

            ClientTest.searchByTitleAndYear();

        } catch (Exception e) {

            e.printStackTrace();

        }

    }
}