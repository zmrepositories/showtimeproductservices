package nl.hanze.movieshowtime.beans;

import nl.hanze.movieshowtime.entities.Product;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import java.util.ArrayList;
import java.util.List;

/**
 * beany weany <code>Product</code>
 *
 * @version 1.0
 * @author Michel Tartarotti
 *
 */
@Stateless
@LocalBean
public class ProductBean
{

    @PersistenceContext(unitName="ShowTimeProductServiceUnit", type=PersistenceContextType.TRANSACTION)
    EntityManager manager;


    /**
     * get all products
     *
     * @return List<Product>
     */
    @XmlElementWrapper(name = "products")
    @XmlElement(name = "product")
    @SuppressWarnings("unchecked")
    public List getAll()
    {
        List<Product> products = new ArrayList<Product>();

        Query query = manager.createQuery("SELECT OBJECT(p) FROM nl.hanze.movieshowtime.entities.Product p");
        // temp fix ...
        query.setFirstResult(0);
        query.setMaxResults(500);

        products.addAll(query.getResultList());

        return products;
    }


    /**
     * get all products limited
     *
     * @param limit int
     * @return Product
     */
    @XmlElementWrapper(name = "products")
    @XmlElement(name = "product")
    @SuppressWarnings("unchecked")
    public List getAllWithLimit(int limit)
    {
        List<Product> products = new ArrayList<Product>();

        Query query = manager.createQuery("SELECT OBJECT(p) FROM nl.hanze.movieshowtime.entities.Product p");
        query.setFirstResult(0);
        query.setMaxResults(limit);

        products.addAll(query.getResultList());

        return products;
    }



    /**
     * get a product by id
     *
     * @param id long
     * @return Product
     */
    public Product getById(long id)
    {
        Product product = null;
        try{
            product = manager.find(Product.class, id);

        }catch (Exception e)
        {
            e.printStackTrace();

            product = new Product();
        }
        return product;
    }




    /**
     * get a product by <code>ProductId</code> europe article number
     *
     * @param productId String
     * @return Product
     */
    public Product getByProductId(String productId)
    {
        Product product = null;;
        try{
            Query query = manager.createQuery("SELECT OBJECT(p) FROM nl.hanze.movieshowtime.entities.Product p WHERE p.productId=:product_id");
            query.setParameter("product_id", productId);
            query.setMaxResults(1);

            if(query.getResultList().size() == 1)
            {
                product = (nl.hanze.movieshowtime.entities.Product)query.getSingleResult();
            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }
        return product;
    }




    /**
     * get a product by <code>ean</code> europe article number
     *
     * @param ean long
     * @return Product
     */
    public Product getByEAN(long ean)
    {
        Product product = null;;
        try{
            Query query = manager.createQuery("SELECT OBJECT(p) FROM nl.hanze.movieshowtime.entities.Product p WHERE p.ean=:ean");
            query.setParameter("ean", ean);
            query.setMaxResults(1);

            if(query.getResultList().size() == 1)
            {
                product = (nl.hanze.movieshowtime.entities.Product)query.getSingleResult();
            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }
        return product;
    }



    /**
     *  find a product by title or a part of a title it will retrieve the first matched product by the
     *  search parameter, search is case insensitive
     *
     * @param title String
     * @return Product
     */
    public Product searchByTitle(String title)
    {
        Product product = null;
        try{
            Query query = manager.createNamedQuery("Product.searchOnTitle", Product.class);
            query.setParameter("title", "%"+title+"%");
            query.setMaxResults(1);

            //if(query.getMaxResults() > 0)
            if(query.getResultList().size() == 1)
            {
                product = (nl.hanze.movieshowtime.entities.Product)query.getSingleResult();
            }

        }catch (Exception e)
        {
            e.printStackTrace();
        }
        return product;
    }



    /**
     *  find all products that match on string or substring search is case insensitive
     *
     * @param title String
     * @return List<Product>
     */
    @XmlElementWrapper(name = "products")
    @XmlElement(name = "product")
    @SuppressWarnings("unchecked")
    public List searchAllByTitle(String title)
    {
        List<Product> products = new ArrayList<Product>();
        try{
            Query query = manager.createNamedQuery("Product.searchOnTitle", Product.class);
            query.setParameter("title", "%"+title+"%");

            products.addAll(query.getResultList());

        }catch (Exception e)
        {
            e.printStackTrace();
        }
        return products;
    }



    /**
     *  find a product by title and year or a part of a title it will retrieve the first matched product by the
     *  search parameter, search is case insensitive
     *
     * @param title String
     * @return Product
     */
    public Product searchByTitleAndYear(String title, int year)
    {
        Product product = null;
        try{
            Query query = manager.createNamedQuery("Product.searchOnTitleAndYear", Product.class);
            query.setParameter("title", "%"+title+"%");
            query.setParameter("year", year);
            query.setMaxResults(1);

            //if(query.getMaxResults() > 0)
            if(query.getResultList().size() == 1)
            {
                product = (nl.hanze.movieshowtime.entities.Product)query.getSingleResult();
            }

        }catch (Exception e)
        {
            e.printStackTrace();
        }
        return product;
    }



    /**
     *  find all products that match on string or substring search is case insensitive
     *
     * @param title String
     * @return List<Product>
     */
    @XmlElementWrapper(name = "products")
    @XmlElement(name = "product")
    @SuppressWarnings("unchecked")
    public List searchAllByTitleAndYear(String title, int year)
    {
        List<Product> products = new ArrayList<Product>();
        try{
            Query query = manager.createNamedQuery("Product.searchOnTitleAndYear", Product.class);
            query.setParameter("title", "%"+title+"%");
            query.setParameter("year", year);

            products.addAll(query.getResultList());

        }catch (Exception e)
        {
            e.printStackTrace();
        }
        return products;
    }



    /**
     * create a new product object
     *
     * @param product Product
     * @return Product the new product
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Product create(Product product)
    {
        try {

            // System.out.println(product);

            // TODO yet not working !!....
            // manager.merge(product.getCategories());
            manager.persist(product);

            //force insert to receive the id of the pod cast...
            manager.flush();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return product;
    }


    /**
     * update a product entity
     *
     * @param product Product
     * @return boolean
     */
    public boolean update(Product product)
    {
        boolean status = false;
        if(getByEAN(product.getEan()).exists()){
            try{
                manager.merge(product);
                status = true;

            }catch (Exception e)
            {
                e.printStackTrace();

            }
        }
        return status;
    }


    /**
     * delete a product entity
     *
     * @param id id Product
     * @return boolean
     */

    public boolean deleteById(long id)
    {
        boolean status = false;
        Product product = getById(id);

        if(product.exists()){
            try{
                manager.remove(product);
                status = true;

            }catch (Exception e)
            {
                e.printStackTrace();

            }
        }
        return status;
    }


    /**
     * delete a product entity by ean number
     *
     * @param ean ean Product
     * @return boolean
     */

    public boolean deleteByEan(long ean)
    {
        boolean status = false;
        Product product = getByEAN(ean);

        if(product.exists()){
            try{
                manager.remove(product);
                status = true;

            }catch (Exception e)
            {
                e.printStackTrace();

            }
        }
        return status;
    }


    /**
     * delete a product entity by product id
     *
     * @param productId ean Product
     * @return boolean
     */

    public boolean deleteByProductId(String productId)
    {
        boolean status = false;
        Product product = getByProductId(productId);

        if(product.exists()){
            try{
                manager.remove(product);
                status = true;

            }catch (Exception e)
            {
                e.printStackTrace();

            }
        }
        return status;
    }



    /**
     *
     * @return EntityManager
     */
    public EntityManager getEntityManager()
    {
        return manager;
    }
}
