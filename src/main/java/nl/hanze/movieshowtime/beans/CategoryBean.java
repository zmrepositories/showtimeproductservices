package nl.hanze.movieshowtime.beans;

import nl.hanze.movieshowtime.entities.Category;
import nl.hanze.movieshowtime.entities.Product;

import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Query;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import java.util.ArrayList;
import java.util.List;

/**
 * beany weany <code>Category</code>
 *
 * @version 1.0
 * @author Michel Tartarotti
 *
 */
@LocalBean
@Stateless
public class CategoryBean
{
    @PersistenceContext(unitName="ShowTimeProductServiceUnit", type= PersistenceContextType.TRANSACTION)
    EntityManager manager;



    @XmlElementWrapper(name = "categories")
    @SuppressWarnings("unchecked")
    public List getAll()
    {
        List<Category> categories = new ArrayList<Category>();

        Query query = manager.createQuery("SELECT OBJECT(c) FROM nl.hanze.movieshowtime.entities.Category c");

        categories.addAll(query.getResultList());

        return categories;
    }


    /**
     * get a category by id
     *
     * @param id long
     * @return Movie
     */
    public Category getById(long id)
    {
        Category category;
        try{
            category = manager.find(Category.class, id);

        }catch (Exception e)
        {
            e.printStackTrace();

            category = new Category();
        }
        return category;
    }


    /**
     * create a new category object
     *
     * @param category Category
     * @return Category the new category
     */
    public Category create(Category category)
    {

        try {
            manager.persist(category);
            //force insert to receive the id of the pod cast...
            manager.flush();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return category;
    }


    /**
     * update a category entity
     *
     * @param category Category
     * @return boolean
     */
    public boolean update(Category category)
    {
        boolean status = false;
        if(getById(category.getId()).exists()){
            try{
                manager.merge(category);
                status = true;

            }catch (Exception e)
            {
                e.printStackTrace();

            }
        }
        return status;
    }



    /**
     * delete a category entity
     *
     * @param id id Category
     * @return boolean
     */
    public boolean delete(long id)
    {
        boolean status = false;
        Category category = getById(id);

        if(category.exists()){
            try{
                manager.remove(category);
                status = true;

            }catch (Exception e)
            {
                e.printStackTrace();

            }
        }
        return status;
    }



    /**
     *
     * @return EntityManager
     */
    public EntityManager getEntityManager()
    {
        return manager;
    }
}
