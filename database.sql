SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server versie:                5.5.24-log - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Versie:              9.1.0.4867
-- --------------------------------------------------------

-- Databasestructuur van showtimeproducts wordt geschreven
CREATE DATABASE IF NOT EXISTS `showtimeproducts` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `showtimeproducts`;


-- Structuur van  tabel showtimeproducts.category wordt geschreven
CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(11) unsigned NOT NULL,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumpen data van tabel showtimeproducts.category: ~2 rows (ongeveer)
INSERT INTO `category` (`id`, `category_id`, `name`) VALUES
	(1, 6146, 'Avontuur'),
	(2, 3454, 'Thriller');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;


-- Structuur van  tabel showtimeproducts.product wordt geschreven
CREATE TABLE `product` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`product_id` VARCHAR(200) NOT NULL,
	`ean` BIGINT(20) UNSIGNED NOT NULL,
	`title` VARCHAR(200) NOT NULL,
	`gpc` VARCHAR(50) NOT NULL,
	`availability_code` INT(11) NOT NULL,
	`availability_description` VARCHAR(150) NOT NULL,
	`cond` VARCHAR(20) NOT NULL,
	`price` DECIMAL(10,2) NOT NULL,
	`year` INT(4) NOT NULL,
	`origin` VARCHAR(80) NOT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=31
;

-- Dumpen data van tabel showtimeproducts.product: ~4 rows (ongeveer)
INSERT INTO `product` (`id`, `product_id`, `ean`, `title`, `gpc`, `availability_code`, `availability_description`, `cond`, `price`, `year`) VALUES
	(1, 0, 5051888195819, 'Harry Potter - Complete 8-Film Collection 2', 'dvd', 160, 'Voor zondag 21.00 uur besteld, maandag in huis', 'Nieuw', 3333336.99, 2014),
	(2, 9200000026739197, 5051888195833, 'Harry Potter - Complete 8-Film Collection (Blu-ray)', 'Blu-ray', 160, 'Voor zondag 21.00 uur besteld, maandag in huis', 'Nieuw', 44.99, 2014);


-- Structuur van  tabel showtimeproducts.product_category wordt geschreven
CREATE TABLE IF NOT EXISTS `product_category` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) unsigned NOT NULL,
  `category_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_category_product_product` (`product_id`),
  KEY `FK_category_product_category` (`category_id`),
  CONSTRAINT `FK_category_product_category` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`),
  CONSTRAINT `FK_category_product_product` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumpen data van tabel showtimeproducts.product_category: ~2 rows (ongeveer)
INSERT INTO `product_category` (`id`, `product_id`, `category_id`) VALUES
	(1, 1, 1),
	(2, 2, 1);



-- Structuur van  tabel showtimeproducts.product_seller wordt geschreven
CREATE TABLE IF NOT EXISTS `product_seller` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) unsigned NOT NULL,
  `seller_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_product_seller_product` (`product_id`),
  KEY `FK_product_seller_seller` (`seller_id`),
  CONSTRAINT `FK_product_seller_product` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`),
  CONSTRAINT `FK_product_seller_seller` FOREIGN KEY (`seller_id`) REFERENCES `seller` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumpen data van tabel showtimeproducts.product_seller: ~2 rows (ongeveer)
INSERT INTO `product_seller` (`id`, `product_id`, `seller_id`) VALUES
	(1, 1, 1),
	(2, 2, 1);



-- Structuur van  tabel showtimeproducts.seller wordt geschreven
CREATE TABLE IF NOT EXISTS `seller` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `url` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumpen data van tabel showtimeproducts.seller: ~1 rows (ongeveer)
INSERT INTO `seller` (`id`, `name`, `url`) VALUES
	(1, 'bol', 'http://www.bol.com/nl/index.html');



SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=1;